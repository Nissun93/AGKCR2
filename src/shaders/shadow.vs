#version 400
layout(location=0) in vec4 in_Position;
uniform mat4 depthMVP;
out vec4 xPosition;
void main(void){
	xPosition =  depthMVP * in_Position;
}